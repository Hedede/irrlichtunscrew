// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".

#ifndef __E_IRLICH_ENDR_PASS_SOMETHING_H__
#define __E_IRLICH_ENDR_PASS_SOMETHING_H__

namespace irr::scene {

	//! Enumeration for render passes.
	/** A parameter passed to the registerNodeForRendering() method of the CSceneManager,
	specifying when the node wants to be drawn in relation to the other nodes. */
	enum E_SCENE_NODE_RENDER_PASS
	{
		//! No pass currently active
		ESNRP_NONE =0,

		//! Camera pass. The active view is set up here. The very first pass.
		ESNRP_CAMERA =1,

		//! In this pass, lights are transformed into camera space and added to the driver
		ESNRP_LIGHT =2,

		//! This is used for sky boxes.
		ESNRP_SKY_BOX =4,

		//! All normal objects can use this for registering themselves.
		/** This value will never be returned by
		CSceneManager::getSceneNodeRenderPass(). The scene manager
		will determine by itself if an object is transparent or solid
		and register the object as SNRT_TRANSPARENT or SNRT_SOLD
		automatically if you call registerNodeForRendering with this
		value (which is default). Note that it will register the node
		only as ONE type. If your scene node has both solid and
		transparent material types register it twice (one time as
		SNRT_SOLID, the other time as SNRT_TRANSPARENT) and in the
		render() method call getSceneNodeRenderPass() to find out the
		current render pass and render only the corresponding parts of
		the node. */
		ESNRP_AUTOMATIC =24,

		//! Solid scene nodes or special scene nodes without materials.
		ESNRP_SOLID =8,

		//! Transparent scene nodes, drawn after solid nodes. They are sorted from back to front and drawn in that order.
		ESNRP_TRANSPARENT =16,

		//! Transparent effect scene nodes, drawn after Transparent nodes. They are sorted from back to front and drawn in that order.
		ESNRP_TRANSPARENT_EFFECT =32,

		//! Drawn after the solid nodes, before the transparent nodes, the time for drawing shadow volumes
		ESNRP_SHADOW =64
	};

} // end namespace irr

#endif // __E_IRLICH_ENDR_PASS_SOMETHING_H__
