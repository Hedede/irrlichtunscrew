// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_FILE_SYSTEM_H_INCLUDED__
#define __C_FILE_SYSTEM_H_INCLUDED__

#include "IReferenceCounted.h"
#include "IXMLReader.h"
#include "IReadFile.h"
#include "CFileList.h"
#include "irrArray.h"

namespace irr {
namespace video {
	class IVideoDriver;
} // end namespace video

namespace io {
class IWriteFile;
class IAttributes;

//! FileSystemType: which Filesystem should be used for e.g. browsing
enum EFileSystemType
{
	FILESYSTEM_NATIVE = 0,	// Native OS FileSystem
	FILESYSTEM_VIRTUAL	// Virtual FileSystem
};

//! The FileSystem manages files and archives and provides access to them.
/** It manages where files are, so that modules which use the the IO do not
need to know where every file is located. A file could be in a .zip-Archive or
as file on disk, using the CFileSystem makes no difference to this. */
class CFileSystem : public IReferenceCounted {
public:
	//! constructor
	CFileSystem();

	//! Opens a file for read access.
	/** \param filename: Name of file to open.
	\return Pointer to the created file interface.
	The returned pointer should be dropped when no longer needed.
	See IReferenceCounted::drop() for more information. */
	IReadFile* createAndOpenFile(const io::path& filename);

	//! Creates an IReadFile interface for accessing memory like a file.
	/** This allows you to use a pointer to memory where an IReadFile is requested.
	\param memory: A pointer to the start of the file in memory
	\param len: The length of the memory in bytes
	\param fileName: The name given to this file
	\param deleteMemoryWhenDropped: True if the memory should be deleted
	along with the IReadFile when it is dropped.
	\return Pointer to the created file interface.
	The returned pointer should be dropped when no longer needed.
	See IReferenceCounted::drop() for more information.
	*/
	IReadFile* createMemoryReadFile(void* memory, s32 len, const io::path& fileName, bool deleteMemoryWhenDropped = false);

	//! Creates an IReadFile interface for accessing files inside files
	/** This is useful e.g. for archives.
	\param fileName: The name given to this file
	\param alreadyOpenedFile: Pointer to the enclosing file
	\param pos: Start of the file inside alreadyOpenedFile
	\param areaSize: The length of the file
	\return A pointer to the created file interface.
	The returned pointer should be dropped when no longer needed.
	See IReferenceCounted::drop() for more information.
	*/
	IReadFile* createLimitReadFile(const io::path& fileName, IReadFile* alreadyOpenedFile, long pos, long areaSize);

	//! Creates an IWriteFile interface for accessing memory like a file.
	/** This allows you to use a pointer to memory where an IWriteFile is requested.
		You are responsible for allocating enough memory.
	\param memory: A pointer to the start of the file in memory (allocated by you)
	\param len: The length of the memory in bytes
	\param fileName: The name given to this file
	\param deleteMemoryWhenDropped: True if the memory should be deleted
	along with the IWriteFile when it is dropped.
	\return Pointer to the created file interface.
	The returned pointer should be dropped when no longer needed.
	See IReferenceCounted::drop() for more information.
	*/
	IWriteFile* createMemoryWriteFile(void* memory, s32 len, const io::path& fileName, bool deleteMemoryWhenDropped=false);

	//! Opens a file for write access.
	/** \param filename: Name of file to open.
	\param append: If the file already exist, all write operations are
	appended to the file.
	\return Pointer to the created file interface. 0 is returned, if the
	file could not created or opened for writing.
	The returned pointer should be dropped when no longer needed.
	See IReferenceCounted::drop() for more information. */
	IWriteFile* createAndWriteFile(const io::path& filename, bool append=false);

	//! Returns the string of the current working directory
	const io::path& getWorkingDirectory();

	//! Changes the current Working Directory to the string given.
	//! The string is operating system dependent. Under Windows it will look
	//! like this: "drive:\directory\sudirectory\"
	/** \param newDirectory: A string specifying the new working directory.
	The string is operating system dependent. Under Windows it has
	the form "<drive>:\<directory>\<sudirectory>\<..>". An example would be: "C:\Windows\"
	\return True if successful, otherwise false. */
	bool changeWorkingDirectoryTo(const io::path& newDirectory);

	//! Converts a relative path to an absolute (unique) path, resolving symbolic links
	io::path getAbsolutePath(const io::path& filename) const;

	//! Returns the directory a file is located in.
	/** \param filename: The file to get the directory from */
	io::path getFileDir(const io::path& filename) const;

	//! Returns the base part of a filename, i.e. the name without the directory
	//! part. If no directory is prefixed, the full name is returned.
	/** \param filename: The file to get the basename from
	    \param keepExtension True if filename with extension
	    is returned otherwise everything after the final '.' is removed as well.
	*/
	io::path getFileBasename(const io::path& filename, bool keepExtension=true) const;

	//! flatten a path and file name for example: "/you/me/../." becomes "/you"
	io::path& flattenFilename( io::path& directory, const io::path& root = "/" ) const;

	//! Get the relative filename, relative to the given directory
	path getRelativeFilename(const path& filename, const path& directory) const;

	EFileSystemType setFileListSystem(EFileSystemType listType);

	//! Creates a list of files and directories in the current working directory
	//! and returns it.
	/** \return a Pointer to the created CFileList is returned. After the list has been used
	it has to be deleted using its CFileList::drop() method.
	See IReferenceCounted::drop() for more information. */
	CFileList* createFileList();

	//! Creates an empty filelist
	/** \return a Pointer to the created CFileList is returned. After the list has been used
	it has to be deleted using its CFileList::drop() method.
	See IReferenceCounted::drop() for more information. */
	CFileList* createEmptyFileList(const io::path& path, bool ignoreCase, bool ignorePaths);

	//! determines if a file exists and would be able to be opened.
	bool existFile(const io::path& filename) const;

	//! Creates a XML Reader from a file.
	/** Use createXMLReaderUTF8() if you prefer char* instead of wchar_t*. See IIrrXMLReader for
	more information on how to use the parser.
	\return 0, if file could not be opened, otherwise a pointer to the created
	IXMLReader is returned. After use, the reader
	has to be deleted using its IXMLReader::drop() method.
	See IReferenceCounted::drop() for more information. */
	IXMLReader* createXMLReader(const io::path& filename);

	//! Creates a XML Reader from a file.
	/** Use createXMLReaderUTF8() if you prefer char* instead of wchar_t*. See IIrrXMLReader for
	more information on how to use the parser.
	\return 0, if file could not be opened, otherwise a pointer to the created
	IXMLReader is returned. After use, the reader
	has to be deleted using its IXMLReader::drop() method.
	See IReferenceCounted::drop() for more information. */
	IXMLReader* createXMLReader(IReadFile* file);

	//! Creates a XML Reader from a file.
	/** Use createXMLReader() if you prefer wchar_t* instead of char*. See IIrrXMLReader for
	more information on how to use the parser.
	\return 0, if file could not be opened, otherwise a pointer to the created
	IXMLReader is returned. After use, the reader
	has to be deleted using its IXMLReaderUTF8::drop() method.
	See IReferenceCounted::drop() for more information. */
	IXMLReaderUTF8* createXMLReaderUTF8(const io::path& filename);

	//! Creates a XML Reader from a file.
	/** Use createXMLReader() if you prefer wchar_t* instead of char*. See IIrrXMLReader for
	more information on how to use the parser.
	\return 0, if file could not be opened, otherwise a pointer to the created
	IXMLReader is returned. After use, the reader
	has to be deleted using its IXMLReaderUTF8::drop() method.
	See IReferenceCounted::drop() for more information. */
	IXMLReaderUTF8* createXMLReaderUTF8(IReadFile* file);

	//! Creates a new empty collection of attributes, usable for serialization and more.
	/** \param driver: Video driver to be used to load textures when specified as attribute values.
	Can be null to prevent automatic texture loading by attributes.
	\return Pointer to the created object.
	If you no longer need the object, you should call IAttributes::drop().
	See IReferenceCounted::drop() for more information. */
	IAttributes* createEmptyAttributes(video::IVideoDriver* driver = 0);

private:
	//! Currently used FileSystemType
	EFileSystemType FileSystemType;
	//! WorkingDirectory for Native and Virtual filesystems
	io::path WorkingDirectory [2];
};


} // end namespace irr
} // end namespace io

#endif

