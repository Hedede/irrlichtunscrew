// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".

#ifndef __C_MESH_SCENE_NODE_H_INCLUDED__
#define __C_MESH_SCENE_NODE_H_INCLUDED__

#include "ISceneNode.h"
#include "IMesh.h"

namespace irr {
namespace scene {

class CShadowVolumeSceneNode;

	class CMeshSceneNode : public ISceneNode {
	public:

		//! constructor
		CMeshSceneNode(IMesh* mesh, ISceneNode* parent, CSceneManager* mgr,	s32 id,
			const core::vector3df& position = core::vector3df(0,0,0),
			const core::vector3df& rotation = core::vector3df(0,0,0),
			const core::vector3df& scale = core::vector3df(1.0f, 1.0f, 1.0f));

		//! destructor
		virtual ~CMeshSceneNode();

		//! frame
		virtual void OnRegisterSceneNode();

		//! renders the node.
		virtual void render();

		//! returns the axis aligned bounding box of this node
		virtual const core::aabbox3d<f32>& getBoundingBox() const;

		//! returns the material based on the zero based index i. To get the amount
		//! of materials used by this scene node, use getMaterialCount().
		//! This function is needed for inserting the node into the scene hirachy on a
		//! optimal position for minimizing renderstate changes, but can also be used
		//! to directly modify the material of a scene node.
		virtual video::SMaterial& getMaterial(u32 i);
		
		//! returns amount of materials used by this scene node.
		virtual u32 getMaterialCount() const;


		//! Returns type of the scene node
		virtual ESCENE_NODE_TYPE getType() const { return ESNT_MESH; }

		//! Sets a new mesh
		void setMesh(IMesh* mesh);

		//! Returns the current mesh
		IMesh* getMesh(void) { return Mesh; }

		//! Creates shadow volume scene node as child of this node
		//! and returns a pointer to it.
		virtual CShadowVolumeSceneNode* addShadowVolumeSceneNode(const IMesh* shadowMesh,
			s32 id, bool zfailmethod=true, f32 infinity=10000.0f);

		//! Sets if the scene node should not copy the materials of the mesh but use them in a read only style.
		/* In this way it is possible to change the materials a mesh causing all mesh scene nodes 
		referencing this mesh to change too. */
		void setReadOnlyMaterials(bool readonly);

		//! Returns if the scene node should not copy the materials of the mesh but use them in a read only style
		bool isReadOnlyMaterials() const;

		//! Removes a child from this scene node.
		//! Implemented here, to be able to remove the shadow properly, if there is one,
		//! or to remove attached childs.
		virtual bool removeChild(ISceneNode* child);

	protected:

		void copyMaterials();

		core::array<video::SMaterial> Materials;
		core::aabbox3d<f32> Box;
		video::SMaterial ReadOnlyMaterial;

		IMesh* Mesh;
		CShadowVolumeSceneNode* Shadow;

		s32 PassCount;
		bool ReadOnlyMaterials;
	};

} // end namespace scene
} // end namespace irr

#endif
