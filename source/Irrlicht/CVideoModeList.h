// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".

#ifndef __IRR_C_VIDEO_MODE_LIST_H_INCLUDED__
#define __IRR_C_VIDEO_MODE_LIST_H_INCLUDED__

#include "IReferenceCounted.h"
#include "dimension2d.h"
#include "irrArray.h"

namespace irr
{
namespace video
{

	class CVideoModeList : public IReferenceCounted
	{
	public:

		//! constructor
		CVideoModeList();

		//! Gets amount of video modes in the list.
		/** \return Returns amount of video modes. */
		s32 getVideoModeCount() const;

		//! Returns the screen size of a video mode in pixels.
		/** \param modeNumber: zero based index of the video mode.
		\return Size of screen in pixels of the specified video mode. */
		core::dimension2d<u32> getVideoModeResolution(s32 modeNumber) const;

		//! Returns the screen size of an optimal video mode in pixels.
		/** \param minSize: Minimum dimensions required.
		\param maxSize: Maximum dimensions allowed.
		\return Size of screen in pixels which matches the requirements.
		as good as possible. */
		core::dimension2d<u32> getVideoModeResolution(const core::dimension2d<u32>& minSize, const core::dimension2d<u32>& maxSize) const;

		//! Returns the pixel depth of a video mode in bits.
		/** \return Size of each pixel of the specified video mode in bits. */
		s32 getVideoModeDepth(s32 modeNumber) const;

		//! Returns current desktop screen resolution.
		const core::dimension2d<u32>& getDesktopResolution() const;

		//! Returns the pixel depth of a video mode in bits.
		/** \return Size of each pixel of the current desktop video mode in bits. */
		s32 getDesktopDepth() const;

		//! adds a new mode to the list
		void addMode(const core::dimension2d<u32>& size, s32 depth);

		void setDesktop(s32 desktopDepth, const core::dimension2d<u32>& desktopSize);

	private:

		struct SVideoMode
		{
			core::dimension2d<u32> size;
			s32 depth;

			bool operator==(const SVideoMode& other) const
			{
				return size == other.size && depth == other.depth;
			}

			bool operator <(const SVideoMode& other) const
			{
				return (size.Width < other.size.Width ||
					(size.Width == other.size.Width &&
					size.Height < other.size.Height) ||
					(size.Width == other.size.Width &&
					size.Height == other.size.Height &&
					depth < other.depth));
			}
		};

		core::array<SVideoMode> VideoModes;
		SVideoMode Desktop;
	};

} // end namespace video
} // end namespace irr


#endif

