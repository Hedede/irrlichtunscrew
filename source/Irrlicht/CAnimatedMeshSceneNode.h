// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".

#ifndef __C_ANIMATED_MESH_SCENE_NODE_H_INCLUDED__
#define __C_ANIMATED_MESH_SCENE_NODE_H_INCLUDED__

#include "SAnimatedMesh.h"
#include "ISceneNode.h"
#include "IBoneSceneNode.h"

#include "matrix4.h"


namespace irr {
namespace scene {
	class CShadowVolumeSceneNode;

	enum E_JOINT_UPDATE_ON_RENDER
	{
		//! do nothing
		EJUOR_NONE = 0,

		//! get joints positions from the mesh (for attached nodes, etc)
		EJUOR_READ,

		//! control joint positions in the mesh (eg. ragdolls, or set the animation from animateJoints() )
		EJUOR_CONTROL
	};

	class CAnimatedMeshSceneNode;

	//! Callback interface for catching events of ended animations.
	/** Implement this interface and use
	CAnimatedMeshSceneNode::setAnimationEndCallback to be able to
	be notified if an animation playback has ended.
	**/
	class IAnimationEndCallBack : public virtual IReferenceCounted
	{
	public:

		//! Will be called when the animation playback has ended.
		/** See CAnimatedMeshSceneNode::setAnimationEndCallback for
		more informations.
		\param node: Node of which the animation has ended. */
		virtual void OnAnimationEnd(CAnimatedMeshSceneNode* node) = 0;
	};


	class IDummyTransformationSceneNode;
	class CAnimatedMeshSceneNode : public ISceneNode
	{
	public:

		//! constructor
		CAnimatedMeshSceneNode(SAnimatedMesh* mesh, ISceneNode* parent, CSceneManager* mgr,	s32 id,
			const core::vector3df& position = core::vector3df(0,0,0),
			const core::vector3df& rotation = core::vector3df(0,0,0),
			const core::vector3df& scale = core::vector3df(1.0f, 1.0f, 1.0f));

		//! destructor
		virtual ~CAnimatedMeshSceneNode();

		//! sets the current frame. from now on the animation is played from this frame.
		virtual void setCurrentFrame(f32 frame);

		//! frame
		virtual void OnRegisterSceneNode();

		//! OnAnimate() is called just before rendering the whole scene.
		virtual void OnAnimate(u32 timeMs);

		//! renders the node.
		virtual void render();

		//! returns the axis aligned bounding box of this node
		virtual const core::aabbox3d<f32>& getBoundingBox() const;

		//! sets the frames between the animation is looped.
		//! the default is 0 - MaximalFrameCount of the mesh.
		bool setFrameLoop(s32 begin, s32 end);

		//! Sets looping mode which is on by default. If set to false,
		//! animations will not be looped.
		void setLoopMode(bool playAnimationLooped);

		//! returns the current loop mode
		bool getLoopMode() const;

		//! Sets a callback interface which will be called if an animation
		//! playback has ended. Set this to 0 to disable the callback again.
		void setAnimationEndCallback(IAnimationEndCallBack* callback=0);

		//! sets the speed with which the animation is played
		void setAnimationSpeed(f32 framesPerSecond);

		//! gets the speed with which the animation is played
		f32 getAnimationSpeed() const;

		//! returns the material based on the zero based index i. To get the amount
		//! of materials used by this scene node, use getMaterialCount().
		//! This function is needed for inserting the node into the scene hirachy on a
		//! optimal position for minimizing renderstate changes, but can also be used
		//! to directly modify the material of a scene node.
		virtual video::SMaterial& getMaterial(u32 i);

		//! returns amount of materials used by this scene node.
		virtual u32 getMaterialCount() const;

		//! Creates shadow volume scene node as child of this node
		//! and returns a pointer to it.
		CShadowVolumeSceneNode* addShadowVolumeSceneNode(const IMesh* shadowMesh,
			s32 id, bool zfailmethod=true, f32 infinity=1000.0f);

		//! Returns a pointer to a child node, which has the same transformation as
		//! the corrsesponding joint, if the mesh in this scene node is a skinned mesh.
		IBoneSceneNode* getJointNode(const c8* jointName);

		//! same as getJointNode(const c8* jointName), but based on id
		IBoneSceneNode* getJointNode(u32 jointID);

		//! Gets joint count.
		u32 getJointCount() const;

		//! Removes a child from this scene node.
		//! Implemented here, to be able to remove the shadow properly, if there is one,
		//! or to remove attached childs.
		virtual bool removeChild(ISceneNode* child);

		//! Returns the current displayed frame number.
		f32 getFrameNr() const;
		//! Returns the current start frame number.
		s32 getStartFrame() const;
		//! Returns the current end frame number.
		s32 getEndFrame() const;

		//! Sets if the scene node should not copy the materials of the mesh but use them in a read only style.
		/* In this way it is possible to change the materials a mesh causing all mesh scene nodes
		referencing this mesh to change too. */
		void setReadOnlyMaterials(bool readonly);

		//! Returns if the scene node should not copy the materials of the mesh but use them in a read only style
		bool isReadOnlyMaterials() const;

		//! Sets a new mesh
		void setMesh(SAnimatedMesh* mesh);

		//! Returns the current mesh
		SAnimatedMesh* getMesh(void) { return Mesh; }


		//! Returns type of the scene node
		ESCENE_NODE_TYPE getType() const { return ESNT_ANIMATED_MESH; }

		//! updates the absolute position based on the relative and the parents position
		void updateAbsolutePosition() override;


		//! Set the joint update mode (0-unused, 1-get joints only, 2-set joints only, 3-move and set)
		void setJointMode(E_JOINT_UPDATE_ON_RENDER mode);

		//! Sets the transition time in seconds (note: This needs to enable joints, and setJointmode maybe set to 2)
		//! you must call animateJoints(), or the mesh will not animate
		void setTransitionTime(f32 Time);

		//! updates the joint positions of this mesh
		void animateJoints(bool CalculateAbsolutePositions=true);

		//! render mesh ignoring its transformation. Used with ragdolls. (culling is unaffected)
		void setRenderFromIdentity( bool On );

	private:

		//! Get a static mesh for the current frame of this animated mesh
		IMesh* getMeshForCurrentFrame();

		void buildFrameNr(u32 timeMs);
		void checkJoints();
		void beginTransition();

		core::array<video::SMaterial> Materials;
		core::aabbox3d<f32> Box;
		SAnimatedMesh* Mesh;

		s32 StartFrame;
		s32 EndFrame;
		f32 FramesPerSecond;
		f32 CurrentFrameNr;

		u32 LastTimeMs;
		u32 TransitionTime; //Transition time in millisecs
		f32 Transiting; //is mesh transiting (plus cache of TransitionTime)
		f32 TransitingBlend; //0-1, calculated on buildFrameNr

		//0-unused, 1-get joints only, 2-set joints only, 3-move and set
		E_JOINT_UPDATE_ON_RENDER JointMode;
		bool JointsUsed;

		bool Looping;
		bool ReadOnlyMaterials;
		bool RenderFromIdentity;

		IAnimationEndCallBack* LoopCallBack;
		s32 PassCount;

		CShadowVolumeSceneNode* Shadow;

		core::array<IBoneSceneNode* > JointChildSceneNodes;
		core::array<core::matrix4> PretransitingSave;
	};

} // end namespace scene
} // end namespace irr

#endif
